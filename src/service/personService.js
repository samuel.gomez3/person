const axios = require('axios');

const subscribeOffers = async (email) => {
  try {
    const response = await axios.post('http://localhost:3001/notification/subscribe', {
      email: email
    });

    console.log('Response:', response.data);
  } catch (error) {
    console.error('Error:', error.response ? error.response.data : error.message);
  }
};


module.exports = subscribeOffers