const express = require('express');
const { getPersons, addOrUpdatePerson, deletePerson, getPersonById } = require('./db/dynamo');
const sendLog = require('./aws/SQS');
const subscribeOffers = require('./service/personService');
const app = express();

app.use(express.json());

app.get('/', (req, res) => {
    res.send('Health')
});

app.get('/persons', async (req, res) => {
    try {
        const persons = await getPersons();
        res.json(persons)
        sendLog(persons.Count + " persons were obtained", "INFO")
    } catch (err) {
        sendLog(err, "ERROR")
        res.status(500).json({err: "something went wrong"})
    }
});


app.get('/persons/:id', async (req, res) => {
    const id = req.params.id;
    try {
        const persons = await getPersonById(id);
        res.json(persons)
        sendLog( "person with id" + id + "has been consulted ", "INFO")
    } catch (err) {
        sendLog(err, "ERROR")
        res.status(500).json({err: "something went wrong"})
    }
});

app.post('/persons', async (req, res) => {
    const person = req.body;

    try {
        const newPerson = await addOrUpdatePerson(person);
        sendLog( "person " + person.name + "was created ", "INFO")
        subscribeOffers(person.email)
        sendLog( "person " + person.name + " subscribed to the offers ", "INFO")
        res.json(newPerson)
        
    } catch (err) {
        sendLog(err, "ERROR")
        res.status(500).json({err: "something went wrong"})
    }

});

app.put('/persons/:id', async (req, res) => {
    const person = req.body;
    const {id} = req.params;
    person.id = id
    try {
        const updatePerson = await addOrUpdatePerson(person);
        res.json(updatePerson)
        sendLog( "person " + id + "was update ", "INFO")
    } catch (err) {
        sendLog(err, "ERROR")
        res.status(500).json({err: "something went wrong"})
    }

});

app.delete('/persons/:id', async(req, res)=>{
    const{id} = req.params;
    try {
        res.json(await deletePerson(id));
        sendLog( "person " + id + "was delete ", "INFO")
    } catch (err) {
        endLog(err, "ERROR")
        res.status(500).json({err: "something went wrong"})
    }
});


const port = process.env.PORT || 3000;
const server = app.listen(port, () => {
    console.log(`listening on port ${port}`);
});

module.exports = app;