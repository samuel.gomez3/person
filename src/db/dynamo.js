const AWS = require('aws-sdk');
require('dotenv').config();

AWS.config.update({
    region: process.env.AWS_DEFAULT_REGION,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY 
});

const dynamoClient = new AWS.DynamoDB.DocumentClient();
const TABLE_NAME = "person"

const getPersons = async ()=> {
    const params = {
        TableName:  TABLE_NAME
    };
    const persons = await dynamoClient.scan(params).promise();
    console.log(persons)
    return persons;
}

const addOrUpdatePerson = async (person) =>{
    const params = {
        TableName: TABLE_NAME,
        Item: person
    }

    return await dynamoClient.put(params).promise();
}

const getPersonById = async (id) => {
    const params = {
        TableName: TABLE_NAME,
        Key: {
            id
        }
    }
    return await dynamoClient.get(params).promise();
}

const deletePerson = async (id) => {
    const params = {
        TableName: TABLE_NAME,
        Key:{
            id
        } 
    };
    return await dynamoClient.delete(params).promise();
}

module.exports = {
    dynamoClient,
    getPersons,
    getPersonById,
    addOrUpdatePerson,
    deletePerson
}