const AWS = require("aws-sdk");

AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: "us-east-1",
});

const sqs = new AWS.SQS();

const sendLog= async (log, level)=> {
    logFormado = new Date() + " "+ level +" " + log
    sqs.sendMessage(
        {
          MessageBody: logFormado,
          QueueUrl: "https://sqs.us-east-1.amazonaws.com/906647043531/logs",
        },
        (err, data) => {
          if (err) {
            console.error("Error al enviar el mensaje", err);
          } else {
            console.log("Mensaje enviado:", data.MessageId);
          }
        }
      );

}


module.exports = sendLog;


