const AWS = require('aws-sdk');

// Crear mock para DocumentClient
const mockDocumentClient = {
    scan: jest.fn(),
    get: jest.fn(),
    put: jest.fn(),
    delete: jest.fn()
};

// Crear mock para DynamoDB ANTES de requerir el módulo dynamo
AWS.DynamoDB.DocumentClient = jest.fn(() => mockDocumentClient);

const {
    getPersons,
    getPersonById,
    addOrUpdatePerson,
    deletePerson
} = require('../src/db/dynamo');

describe('Person Controller', () => {
    beforeEach(() => {
        // Limpiar mocks antes de cada prueba
        jest.clearAllMocks();
    });

    it('should get all persons', async () => {
        const mockPersons = { Items: [{ id: '1', name: 'John' }] };
        mockDocumentClient.scan.mockReturnValue({
            promise: jest.fn().mockResolvedValueOnce(mockPersons)
        });

        const result = await getPersons();
        expect(result).toEqual(mockPersons);
        expect(mockDocumentClient.scan).toHaveBeenCalledTimes(1);
    });

    it('should get person by id', async () => {
        const mockPerson = { Item: { id: '1', name: 'John' } };
        mockDocumentClient.get.mockReturnValue({
            promise: jest.fn().mockResolvedValueOnce(mockPerson)
        });

        const result = await getPersonById('1');
        expect(result).toEqual(mockPerson);
        expect(mockDocumentClient.get).toHaveBeenCalledTimes(1);
    });

    it('should add or update a person', async () => {
        mockDocumentClient.put.mockReturnValue({
            promise: jest.fn().mockResolvedValueOnce({})
        });

        const mockPerson = { id: '1', name: 'John' };
        await addOrUpdatePerson(mockPerson);
        
        expect(mockDocumentClient.put).toHaveBeenCalledTimes(1);
    });

    it('should delete a person', async () => {
        mockDocumentClient.delete.mockReturnValue({
            promise: jest.fn().mockResolvedValueOnce({})
        });

        await deletePerson('1');
        expect(mockDocumentClient.delete).toHaveBeenCalledTimes(1);
    });
});
